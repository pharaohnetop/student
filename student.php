<?php

//app/student/th.php
$messages = array(
	//edit buttons
	"chooseStudent"    => "เลือกนักเรียน",
	"addStudent"    => "เพิ่มนักเรียน",
	"deleteStudent"    => "ลบนักเรียน",
	
	//student data
    "studentCode"      => "เลขประจำตัวนักเรียน",
    "citizenID"     => "เลขประจำตัวประชาชน",
    "prefixName" => "คำนำหน้า",
    "firstName"    => "ชื่อ",
    "lastName"    => "นามสกุล",
	"fatherFirstname"    => "ชื่อบิดา",
	"fatherLastname"    => "นามสกุลบิดา",
	"fatherCitizenID"    => "เลขบัตรประจำตัวบิดา",
	"matherFirstname"    => "ชื่อมารดา",
	"motherLastname"    => "นามสกุลมารดา",
	"motherCitizenID"    => "เลขบัตรประจำตัวมารดา",
	"parentFirstname"    => "ชื่อผู้ปกครอง",
	"parentLastname"    => "นามสกุลผู้ปกครอง",
	"parentCitizenID"    => "เลขประจำตัวผู้ปกครอง",
	
	//education tab
	"dateofAdmission"    => "วันที่เข้าศึกษา",
	"dateofApprove"    => "วันที่อนุมัติจบ",
	"dateofGradute"    => "วันที่จบ",
	"pastSchool"    => "โรงเรียนเดิม",
	"gradepointAverage"    => "GPA",
	"credit"    => "หน่วยกิต",
	"thai"    => "ไทย",
	"mathematic"    => "คณิตศาสตร์",
	"science"    => "วิทยาศาสตร์",
	"socialStudies"    => "สังคมศึกษา",
	"healthEducation"    => "สุขศึกษา",
	"artEducation"    => "ศิลปะศึกษา",
	"careerEducation"    => "การงานอาชีพ",
	"english"    => "ภาษาอังกฤษ",
	"result"    => "ผลฯ",
	
	//address tab
	"addressNumber"    => "รหัสประจำบ้าน",
	"addressName"    => "ที่อยู่",
	"alleyName"    => "ซอย",
	"streetName"    => "ถนน",
	"postalCode"    => "รหัสไปรษณีย์",
	"phoneNumber"   => "เบอร์โทรศัพท์",
	
	
);

//app/student/en.php
$messages = array(
	//edit buttons
	"chooseStudent"    => "Choose Student",
	"addStudent"    => "Add Student",
	"deleteStudent"    => "Delete Student",
	
	
	
	//student data
    "studentCode"      => "Student Id",
    "citizenID"     => "Citizen Id",
    "prefixName" => "Prefix",
    "firstName"    => "First Name",
    "lastName"    => "Last Name",
	"fatherFirstname"    => "Student Father's name",
	"fatherLastname"    => "Student Father's last name",
	"fatherCitizenID"    => "Student Father's Citizen ID",
	"motherFirstname"    => "Student Mother's name",
	"motherLastname"    => "Student Mother's last name",
	"motherCitizenID"    => "Student Mother's Citizen ID",
	"parentFirstname"    => "Student Parent's name",
	"parentLastname"    => "Student Parent's last name",
	"parentCitizenID"    => "Student Parent's Citizen ID",
	
	//education tab
	"dateofAdmission"    => "Date of addmission",
	"dateofApprove"    => "Date of approve",
	"dateofGradute"    => "Date of graduate",
	"pastSchool"    => "Past School",
	"gradepointAverage"    => "GPA",
	"credit"    => "Credits",
	"thai"    => "Thai",
	"mathematics"    => "Mathematics",
	"science"    => "Science",
	"socialStudies"    => "Social Studies",
	"healthEducation"    => "Health Education",
	"artEducation"    => "Art Education",
	"careerEducation"    => "Career Education",
	"english"    => "English",
	"result"    => "Result",
	
	//address tab
	"addressNumber"    => "Address No.",
	"addressName"    => "Address",
	"alleyName"    => "Alley",
	"streetName"    => "Street",
	"postalCode"    => "Postal code",
	
	
);