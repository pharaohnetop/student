<?php


$messages = array(
	//edit buttons
	"chooseSchooyear"    => "เลือกปีการศึกษา",
	"chooseClass"    => "เลือกชั้น",
	"chooseRoom"    => "เลือกห้อง",
	
	//education tab
	"studentCode"    => "รหัส",
	"studentName"    => "ชื่อ",
	"studentLastname"    => "สกุล",
	"dateofApprove"    => "วันที่อนุมัติ",
	"dateofGraduate"    => "วันที่จบ",
	"reasonforleave"	=> "สาเหตุที่ออก",
	"note"    => "หมายเหตุ",
	
	//result tab
	"studentCode"    => "รหัส",
	"studentName"    => "ชื่อ",
	"studentLastname"    => "สกุล",
	"readResult" 	=> "ผลการอ่าน",
	"attributeResult" 	=> "ผลคุณลักษณะ"
	"activityResult" 	=> "ผลกิจกรรม"
	"base" 	=> "พื้นฐาน"
	"increase" 	=> "เพิ่มเติม"
	"reading" 	=> "การอ่าน"
	"attribute" 	=> "คุณลักษณะ"
	"activity" 	=> "กิจกรรม"
	
	//O-net point tab
	"studentCode"    => "รหัส",
	"studentName"    => "ชื่อ",
	"studentLastname"    => "สกุล",
	"thai" 	=> "ไทย"
	"mathematic" 	=> "คณิตฯ"
	"science" 	=> "วิทย์"
	"socialStudies"    => "สังคม",
	"healthEducation"    => "สุขศึกษา",
	"artEducation"    => "ศิลปะศึกษา",
	"careerEducation"    => "การงาน",
	"english"    => "ภาษาอังกฤษ",
	"result"    => "ผลฯ",
	
);


$messages = array(
	//edit buttons
	"chooseSchooyear"    => "Choose School year",
	"chooseClass"    => "Choose Class",
	"chooseRoom"    => "Choose Room",
	
	//education tab
	"studentCode"    => "Code",
	"studentName"    => "Name",
	"studentLastname"    => "Last name",
	"dateofApprove"    => "Date of approve",
	"dateofGraduate"    => "Date of graduate",
	"reasonforleave"	=> "Reason for leaving",
	"note"    => "Note",
	
	//result tab
	"studentCode"    => "Code",
	"studentName"    => "Name",
	"studentLastname"    => "Last Name",
	"readResult" 	=> "Read result",
	"attributeResult" 	=> "Attribute result"
	"activityResult" 	=> "Activity result"
	"Primary" 	=> "Primary"
	"increase" 	=> "Increase"
	"reading" 	=> "Reading"
	"attribute" 	=> "Attribute"
	"activity" 	=> "Activity"
	
	//O-net point tab
	"studentCode"    => "Code",
	"studentName"    => "Name",
	"studentLastname"    => "Last name",
	"thai" 	=> "Thai"
	"mathematic" 	=> "Mathematics"
	"science" 	=> "Science"
	"socialStudies"    => "Social",
	"healthEducation"    => "Health",
	"artEducation"    => "Art",
	"careerEducation"    => "Career",
	"english"    => "English",
	"result"    => "Result",
	
	
	
	
	
	
);