<?php

//app/student/th.php
$messages = array(
	//edit buttons
	"chooseTeacher"    => "เลือกอาจารย์",
	"addTeacher"    => "เพิ่มอาจารย์",
	"deleteTeacher"    => "ลบอาจารย์",
	
	
	//teacher data
	"teacherCode"    => "เลขประจำตัวอาจารย์",
	"citizenID"    => "เลขประจำตัวประชาขน",
    "prefixName" => "คำนำหน้า",
    "firstName"    => "ชื่อ",
    "lastName"    => "นามสกุล",
	"dateofBirth"    => "วันเกิด",
	"dateofAttendance"    => "วันที่เริ่มงาน",
	"firstdateofSchool"		=> "วันที่เริ่มสอน",
	"position"		=> "ตำแหน่ง",
	"mainSubject"		=> "วิชาที่สอน",
	"salary"		=> "เงินเดือน",
	"specialization"		=> "ความเชี่ยวชาญ",
	
	
	
	
	//address tab
	"addressNumber"    => "รหัสประจำบ้าน",
	"addressName"    => "ที่อยู่",
	"alleyName"    => "ซอย",
	"streetName"    => "ถนน",
	"postalCode"    => "รหัสไปรษณีย์",
	"phoneNumber"   => "เบอร์โทรศัพท์",
	
	
);

//app/student/en.php
$messages = array(
	//edit buttons
	"chooseTeacher"    => "Choose Teacher",
	"addTeacher"    => "Add Teacher",
	"deleteTeacher"    => "Delelte Teacher",
	
	
	//teacher data
	"teacherCode"    => "Teacher code",
	"citizenID"    => "Citizen Id",
    "prefixName" => "",
    "firstName"    => "Name",
    "lastName"    => "Last name",
	"dateofBirth"    => "Date of birth",
	"dateofAttendance"    => "Date of attendance",
	"firstdateofSchool"		=> "First date of school",
	"position"		=> "Position",
	"mainSubject"		=> "Main Subject",
	"salary"		=> "Salary",
	"specialization"		=> "Specialization",
	
	
	
	
	//address tab
	"addressNumber"    => "Address",
	"addressName"    => "Adress Name",
	"alleyName"    => "Alley",
	"streetName"    => "Street",
	"postalCode"    => "Postal Code",
	"phoneNumber"   => "Phone number",
	
	
	
);