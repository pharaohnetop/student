<?php

//app/student/th.php
$messages = array(
	//edit buttons
	"addData"    => "เพิ่มข้อมูล",
	"save"    => "บันทึก",
	"cancel"    => "ยกเลิก",
	
	
	//subject data
	"subjectCode"    => "รหัสวิชา",
	"reportCode"    => "รหัสออกรายงาน",
	"subjectName"    => "ชื่อวิชา",
	"subjectNameeng"    => "รหัสวิชา(Eng)",
	"subjectCodeeng"    => "รหัสวิชา(Eng)",
	"credit"    => "หน่วยกิต",
	"hour"    => "ชั่วโมงเต็ม",
	"hourperWeek"    => "ชั่วโมงต่อสัปดาห์",
	
	
	
	
);

//app/student/en.php
$messages = array(
	//edit buttons
	"addData"    => "Add data",
	"save"    => "Save",
	"cancel"    => "Cancel",
	
	
	//subject data
	"subjectCode"    => "Subject Code",
	"reportCode"    => "Code for report",
	"subjectName"    => "Subject Name",
	"subjectNameeng"    => "รหัสวิชา(Eng)",
	"subjectCodeeng"    => "รหัสวิชา(Eng)",
	"credit"    => "Credits",
	"hour"    => "Hour",
	"hourperWeek"    => "Hours per week",
	
	
	
	
	
	
	
);