<?php


$messages = array(
	//edit buttons
	"chooseSchooyear"    => "เลือกปีการศึกษา",
	"chooseClass"    => "เลือกชั้น",
	"chooseRoom"    => "เลือกห้อง",
	
	"studentCode"    => "รหัส",
	"studentName"    => "ชื่อ",
	"studentLastname"    => "สกุล",

	//student data field 
	"title"    => "คำนำหน้าชื่อ",
	"studentName"    => "ชื่อ",
	"studentLastname"    => "สกุล",
	"birthDay"    => "วันเกิด",
	"gender"    => "เพศ",
	"nationality"    => "สัญชาติ",
	"race"    => "เชื้อชาติ",
	"religion"    => "ศาสนา",
	"citizenID"    => "เลขประชาชน",
	"fatherTitle"    => "คำนำหน้าบิดา",
	"fatherFirstname"    => "ชื่อบิดา",
	"fatherLastname"    => "นามสกุลบิดา",
	"motherTitle"    => "คำนำหน้ามารดา",
	"matherFirstname"    => "ชื่อมารดา",
	"motherLastname"    => "นามสกุลมารดา",
	"dateofAdmission"    => "วันที่เข้าศึกษา",
	"dateofApproval"    => "วันที่อนุมัติจบ",
	"dateofGraduate"    => "วันที่จบ",
	"pastSchool"    => "โรงเรียนเดิม",
	"lastYear"    => "ชั้นปีสุดท้าย",
	"province"    => "จังหวัด",
	"reasonforleave"    => "เหตุที่ออก",
	"studiedCredit"    => "หน่วยที่เรียน",
	"credit"    => "หน่วยที่ได้",
	"GPA"    => "GPA",
	"learnactivity"    => "กิจกรรมที่เรียน",
	"credit"    => "หน่วยที่ได้",
	
	
	
	
);


$messages = array(
		//edit buttons
	"chooseSchooyear"    => "Choose School year",
	"chooseClass"    => "Choose Class",
	"chooseRoom"    => "Choose Room",
	
	"studentCode"    => "Code",
	"studentName"    => "Name",
	"studentLastname"    => "Last name",
	
	//student data field 
	"title"    => "Title",
	"studentName"    => "Name",
	"studentLastname"    => "Last name",
	"birthDate"    => "Birth date",
	"gender"    => "Gender",
	"nationality"    => "Nationality",
	"race"    => "Race",
	"religion"    => "Religion",
	"citizenID"    => "Citizen id",
	"fatherTitle"    => "Student Father's Title",
	"fatherFirstname"    => "Student father's First name",
	"fatherLastname"    => "Student father's Last name",
	"motherTitle"    => "Student Mother's Title",
	"matherFirstname"    => "Student father's First name",
	"motherLastname"    => "Student father's Last name",
	"dateofAdmission"    => "Date of admission",
	"dateofApproval"    => "Date of approval",
	"dateofGraduate"    => "Date of graduate",
	"pastSchool"    => "Past School",
	"lastYear"    => "Last year",
	"province"    => "Province",
	"reasonforleave"    => "Reason for leaving",
	"studiedCredit"    => "Studied Credits",
	"credit"    => "Credits",
	"GPA"    => "GPA",
	"learnactivity"    => "Learning activity",
	"credit"    => "Credits",
	
	
	
	
	

	
	
	
	
	
	
);